#!/bin/env bash

CURRENT_TIME=$(date +%Y-%m-%d-%H-%M-%Z)

fullbak=$CURRENT_TIME-asteriskcdrdb-fullbak.sql
databak=$CURRENT_TIME-asteriskcdrdb-databak.sql
structurebak=$CURRENT_TIME-asteriskcdrdb-structurebak.sql

bak_dir=/var/bak/asteriskcdrbak

mysqldump -uasteriskuser -phongha@321 asteriskcdrdb > $bak_dir/$fullbak
mysqldump -uasteriskuser -phongha@321 --no-create-info asteriskcdrdb > $bak_dir/$databak
mysqldump -uasteriskuser -phongha@321 --no-data asteriskcdrdb > $bak_dir/$structurebak
