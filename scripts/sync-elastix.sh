#!/bin/env bash

logfilename=`date +%Y-%m-%d-%H-%M-%Z`-sync.log
logfile=/var/bak/synclog/$logfilename

sshpass -p "hatinh@" rsync -ar --bwlimit=5120 \
--files-from=/var/bak/bin/included-items.txt \
/ admin@backupht:/volume/elastix-bak/ \
> $logfile 2>&1
