#!/bin/env bash

# THIS VERY SIMPLE AND UGLY SCRIPT

# Following is what this script was intended:
# - Backup a specified MySQL database.
# - Rsync to a remote those location
#
#

# Provides your mysql username, pass, and database
# Oh, your mysql pass, ok, chmod this file 600
MYSQL_USER="asteriskuser"
MYSQL_PASS="hongha@321"
MYSQL_DATABASE="asteriskcdrdb"

# Dump mode, might be data or structure only. I prefer a full backup.
#MYSQL_DUMP_MODE=full
MYSQL_DUMP_OPT=""

case $MYSQL_DUMP_MODE in
    *data*)
        MYSQL_DUMP_OPT="${MYSQL_DUMP_OPT} --no-create-info"
        ;;
    *structure*)
        MYSQL_DUMP_OPT="${MYSQL_DUMP_OPT} --no-data"
        ;;
    *)
        MYSQL_DUMP_OPT="${MYSQL_DUMP_OPT}"
        MYSQL_DUMP_MODE="full"
        ;;
esac

# Specify your ssh acc, and the server: ip or hostname
SSH_USER=samdx
SSH_SERVER=backupht

# Give the destination to sync to, bwlimit, sync opts if there is/are
# The destination is on the SSH_SERVER not local
RSYNC_DEST_DIR="volume1/elastix-bak"
RSYNC_BWLIMIT="5120" # 5MB, unit in KBPS
RSYNC_OPT=""

if [ -n $RSYNC_BWLIMIT ]
then
    RSYNC_OPT="${RSYNC_OPT} --bwlimit=$RSYNC_BWLIMIT"
fi

# Get current datetime
CURRENT_TIME=$(date +%Y-%m-%d-%H-%M-%Z)

# Just a path where this script and/or some other file live on
BACK_DIR="/var/backup"
# Specify the file that contains all the location need to be synced
INCLUDE_FILE_NAME="included-items.txt"
INCLUDE_FILE=$BACK_DIR/$INCLUDE_FILE_NAME
# Name of the dir that mysql dumped files will be stored
ASTERISK_BAK_DIR="asteriskcdrbak"
# The same but for rsync log files
SYNC_LOG_DIR="synclog"

# The sql dumped file name and path
SQL_BACK_FILE="$BACK_DIR/$ASTERISK_BAK_DIR/$CURRENT_TIME-$MYSQL_DATABASE-$MYSQL_DUMP_MODE.sql"
# The sync log file name and path
SYNC_LOG_FILE="$BACK_DIR/$SYNC_LOG_DIR/$CURRENT_TIME.log"

# Dump the database
mysqldump -u$SQL_USER -p$SQL_PASS $SQL_DUMP_OPT \
$SQL_DATABASE > $SQL_BACK_FILE

# Then sync
rsync -ar $RSYNC_OPT --files-from=$INCLUDE_FILE \
/ $SSH_USER@$SSH_SERVER:/$RSYNC_DEST_DIR \
> $SYNC_LOG_FILE 2>&1
