## Roadmap

- Tạo người dùng mới đủ phân quyền chạy sao lưu, trên NAS lẫn Server.
- Tạo thư mục nhằm mục đích dùng lưu trữ sao lưu trên NAS.
- Cấu hình SSH cho người dùng: Passwordless login with publick key.
- \xout{Script để sao lưu dữ liệu `sql`: `mysqldump` `asteriskcdrdb` database}.
- Chỉnh sửa `automatic_backup` dựa vào `backupengine`.
- Script để đồng bộ dữ liệu lên NAS: `rsync`.
- Cho vào `Crond` với `crontab`.


### Quick\&Dirty Guide

- Tạo người dùng và thư mục lưu trữ bộ công cụ sao lưu.
- Sao chép các công cụ vào thư mục trên dữ nguyên cấu trúc.
- Cấp quyền đọc cho người dùng vừa tạo đối với tập tin `/etc/elastix.conf`.
- Gọi `crontab` với tập tin lên lịch định sẵn trong `$backupdir/bin/cron`.
