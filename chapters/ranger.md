### Install `ranger`

```bash
$ git clone git://git.savannah.nongnu.org/ranger.git

# Run the following command with sudo/root permision. 
$ ranger/setup.py install
```

#### How to use?

Eh, try it by yourself.

Have I ever use Vim? Just do it.

#### Preview

![Ranger in Action](../images/ss/20170312-012-ranger.png)