## Cấu hình SSH

**Mục đích:** Cài đặt SSH để người dùng có thể đăng nhập sử dụng mã khoá công khai thay vì sử dụng mật khẩu (rightarrow) tiện lợi và an toàn hơn, sẽ không cần parse mật khẩu vào cho script `rsync` dùng để đồng bộ.


**TODO:**

- Tạo mới keygen
- Cho *public key* vừa tạo vào file `~/.ssh/authorized_keys` trên NAS tương ứng người dùng.

### Tạo SSH key cho người dùng

Sử dụng `ssh-keygen` như ví dụ dưới: tạo mới một mã khoá RSA.

Mục đích sử dụng *publick key* để đăng nhập vào NAS mà không cần mật khẩu, nên tại dòng *Enter passphrase* sẽ bỏ trống, đơn giản là bấm Enter để tiếp tục.

```bash
[admin@elastixht ~]$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/admin/.ssh/id_rsa): 
Created directory '/home/admin/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/admin/.ssh/id_rsa.
Your public key has been saved in /home/admin/.ssh/id_rsa.pub.
The key fingerprint is:
58:82:5c:ff:65:20:3d:af:1f:87:aa:10:5c:e7:e2:d4 admin@elastixht.htbranch
```

### Copy public key vào NAS

#### ssh-copy-id

Sử dụng `ssh-copy-id` theo cú pháp như dưới, mặc định `ssh-copy-id` sẽ sử dụng file `~/.ssh/id_rsa.pub`. Trường hợp muốn chỉ định public key khác, sử dụng tuỳ chọn `-i /path/to/pub_file` khi gọi lệnh.

```text
[samdx@centos-01 ~]$ ssh-copy-id backupht
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s),
to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed --
if you are prompted now it is to install the new keys
samdx@backupht's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'backupht'"
and check to make sure that only the key(s) you wanted were added.

[samdx@centos-01 ~]$
```

#### Các cách khác

Ngoài ra, có thể sử dụng những cách khác để thực hiện việc trên. Mục đích là để copy nội dung của public key vào file `~/.ssh/authorized_key` của người dùng tương ứng trên SSH server, ở đây là NAS.

- Từ WEBUI, dùng Text Editor tạo mới và thêm nội dung cho file `authorized_key`.
- SSH vào NAS với người dùng tương ứng và thêm nội dung cho file.
- SSH vào NAS với một tài khoản khác, chuyển đổi người dùng (`su`)và thực hiện việc thêm nội dung cho file.

Hãy lưu ý về phân quyền của thư mục `~/.ssh` và `authorized_keys`, các lỗi đăng nhập có thể phát sinh từ đây. Phân quyền:

- `700` cho `~/.ssh`: `chmod 700 ~/.ssh`
- `600` cho `authorized_keys`: `chmod 600 ~/.ssh/authorized_keys`

### Đăng nhập bằng public key
\label{sub:public_key_signin}

Thử login:

```bash
[samdx@centos-01 ~]$ ssh backupht
Last login: Sun Mar 12 17:42:36 2017 from 10.0.0.82


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> 

```

Như vậy là đã có thể đăng nhập bằng public key, mà không sử dụng trực tiếp mật khẩu của người dùng. Điều này tạo thuận tiện cho việc gọi `rsync` về sau.

### A bit tweak for SSHD

Chúng ta sẽ làm một vài thay đổi cho SSHD trên Server, áp dụng cho `sshd` cả trên NAS hay trên Elastix.

- Đổi cổng mặc định.
- Tắt đăng nhập bằng người dùng root.
- Giới hạn người dùng có thể đăng nhập bằng SSH.
- Tắt xác thực bằng mật khẩu, bắt buộc dùng Public key.

Sử dụng `vim` hoặc bất kỳ trình soạn thảo nào bạn thuận tay, và sửa file như dưới:

```text
[root@elastixht ~]# vim /etc/sshd/sshd_config
```

```apacheconf
# Use 8822 instead
Port 8822

# Disable root login
RootPermitLogin no

# Allowed users
AllowUsers samdx

PubkeyAuthentication yes

AuthorizedKeysFile .ssh/authorized_keys

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no

# Change to no to disable s/key passwords
ChallengeResponseAuthentication no
```
