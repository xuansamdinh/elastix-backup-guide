## Đồng bộ với RSync
\label{sec:rsync}

Mục này sẽ thực hiện đồng bộ tất cả những dữ liệu cần sao lưu lên server khác, trường hợp này là NAS.

Ở mục \nameref{sub:folder_backup} (trang \pageref{sub:folder_backup}) chúng ta đã tạo thư mục dành cho công việc sao lưu, và mục \nameref{sec:tools} (trang \pageref{sec:tools}) chúng ta đã chuẩn bị các công cụ cho việc sao lưu.

Ta chuyển thư mục làm việc đến `/var/backup/` cùng các công cụ ở mục trước.

Các việc tiến hành:

- Thử `rsync` với public key đã có.
- Lên danh sách các thư mục/item cần sao lưu.
- Tạo script cho việc sao lưu.

### Thử rsync

Sau khi có thể `ssh` sử dụng public key (\nameref{sub:public_key_signin} trang \pageref{sub:public_key_signin}), thử sử dụng `rsync` để đảm bảo rằng không lỗi nào phát sinh liên quan đến việc xác thực khi đăng nhập.

```text
[samdx@elastixht /var/backup]$ echo -e "## WELCOME\n" > README.md
[samdx@elastixht /var/backup]$ rsync -av README.md backupht:/volume1/elastix-bak/
sending incremental file list
README.md

sent 104 bytes  received 31 bytes  90.00 bytes/sec
total size is 12  speedup is 0.09
[samdx@elastixht /var/backup]$ ssh backupht
Last login: Sun Mar 12 18:23:42 2017 from 10.0.0.12


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> ls /volume1/elastix-bak/
README.md
BackupHT> ls -l /volume1/elastix-bak/
total 8
-rw-rw-r-- 1 samdx users   12 2017-03-12 18:28 README.md

```

### Các thư mục cần sao lưu

Danh sách các thư mục cần sao lưu, sẽ được cho vào một tập tin, tạm gọi là `included-items.txt`.

Mục đích sao lưu những thành phần của Elastix, nội dung tập tin sẽ tương tự:

```
/var/spool/asterisk/monitor
/var/www/backup
/var/log/asterisk/cdr-csv
/var/backup

```

Trong đó:

- `/var/spool/asterisk/monitor`: Thư mục chứa các bản ghi âm, thường rất nặng.
- `/var/www/backup`: Những tập tin sao lưu được lệnh từ WEBGUI sẽ lưu tại đây.
- `/var/log/asterisk/cdr-csv`: Bản ghi danh sách cuộc gọi, từ bảng `cdr` của `asteriskcdrdb`.
- `/var/backup`: Thư mục hiện hành, bao gồm các công cụ sao lưu và các bản sao lưu được tạo bởi `automatic_backup.php` đã nói ở trên.

Có thể thêm các thành phần nằm trong thư mục `/var/spool/asterisk`, ví dụ `voicemaill` nếu cần.

### Kịch bản sao lưu

Mục này chúng ta tạo thư mục lưu các kịch bản và các file dành cho việc sao lưu.

- Tạo thư mục `bin` trong `/var/backup`.
- Tạo mới file `automatic_sync.sh` trong thư mục trên.
- Di chuyển các tập tin bao gồm `automatic_backup.php`, `backupengine`, và `included-items.txt`.

```
[samdx@elastixht /var/backup]$ mkdỉr bin
[samdx@elastixht /var/backup]$ mv *.{php,txt} bin/
[samdx@elastixht /var/backup]$ vim bin/automatic_sync.sh
```

#### automatic_sync.sh

Tập tin kịch bản này thực hiện việc đồng bộ các thư mục chỉ định đến đích là một thư mục trên NAS đã được định trước, đủ quyền đọc/ghi.

```bash
#!/bin/env bash

# THIS VERY SIMPLE AND UGLY SCRIPT

# Chỉ định tên người dùng và hostname của NAS
SSH_USER=samdx
SSH_SERVER=backupht

# Các tuỳ chọn cho rsync:
# Thư mục lưu trên NAS
# Giới hạn băng thông nếu cần (Hà Tĩnh 100MB based)
# Các tuỳ chọn khác nếu có
# The destination is on the SSH_SERVER not local
RSYNC_DEST_DIR="volume1/elastix-bak"
RSYNC_BWLIMIT="5120" # 5MB, unit in KBPS
RSYNC_OPT=""

if [ -n $RSYNC_BWLIMIT ]
then
    RSYNC_OPT="${RSYNC_OPT} --bwlimit=$RSYNC_BWLIMIT"
fi

# Get current datetime
CURRENT_TIME=$(date +%Y-%m-%d-%H-%M-%Z)

# The path where this script and/or some other file lives on
BACK_DIR="/var/backup"
# The folder that holds rsync log files
SYNC_LOG_DIR="synclog"

# Specify the file that contains all the location need to be synced
INCLUDE_FILE_NAME="included-items.txt"
INCLUDE_FILE=$BACK_DIR/bin/$INCLUDE_FILE_NAME

# The sync log file name and path
SYNC_LOG_FILE="$BACK_DIR/$SYNC_LOG_DIR/$CURRENT_TIME.log"

# Then sync
rsync -ar $RSYNC_OPT --files-from=$INCLUDE_FILE \
/ $SSH_USER@$SSH_SERVER:/$RSYNC_DEST_DIR \
> $SYNC_LOG_FILE 2>&1

```
