## Đến với RSync

Sau khi có thể `ssh` sử dụng public key, thử sử dụng `rsync` để đảm bảo rằng nó không lỗi nào phát sinh liên quan đến việc xác thực khi đăng nhập.

### Thử rsync

```text
[samdx@elastixht ~]$ echo -e "## WELCOME\n" > README.md
[samdx@elastixht ~]$ rsync -av README.md backupht:/volume1/elastix-bak/
sending incremental file list
README.md

sent 104 bytes  received 31 bytes  90.00 bytes/sec
total size is 12  speedup is 0.09
[samdx@elastixht ~]$ ssh backupht
Last login: Sun Mar 12 18:23:42 2017 from 10.0.0.12


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> ls /volume1/elastix-bak/
README.md  var
BackupHT> ls -l /volume1/elastix-bak/
total 8
-rw-rw-r-- 1 samdx users   12 2017-03-12 18:28 README.md

```

### Các chuẩn bị cho rsync

Mục này chúng ta tạo thư mục lưu các kịch bản và các file dành cho việc sao lưu.
