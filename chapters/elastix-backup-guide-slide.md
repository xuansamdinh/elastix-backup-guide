# ELASTIX BACKING-UP WITH RSYNC

An Essential Guide



---

## Roadmap

- Tạo người dùng mới đủ phân quyền chạy sao lưu, trên NAS lẫn Server.
- Tạo thư mục nhằm mục đích dùng lưu trữ sao lưu trên NAS.
- Cấu hình SSH cho người dùng: Passwordless login with publick key.
- ~~Script để sao lưu dữ liệu `sql`: `mysqldump` `asteriskcdrdb` database~~.
- Chỉnh sửa `automatic_backup` dựa vào `backupengine`.
- Script để đồng bộ dữ liệu lên NAS: `rsync`.
- Cho vào `Crond` với `crontab`

---


### Quick\&Dirty Guide

- Tạo người dùng và thư mục lưu trữ bộ công cụ sao lưu.
- Sao chép các công cụ vào thư mục trên dữ nguyên cấu trúc.
- Cấp quyền đọc cho người dùng vừa tạo đối với tập tin `/etc/elastix.conf`.
- Gọi `crontab` với tập tin lên lịch định sẵn trong `$backupdir/bin/cron`.

---


## Chuẩn bị người dùng và thư mục lưu trữ

Tại mục này chúng ta sẽ hoàn thành việc khởi tạo người dùng mới cho mục đích chạy tác vụ sao lưu. Thực tế, có thể sử dụng ngay tài khoản root trên hệ thống, nhưng điều này có thể không tiện. Tất nhiên cũng có thể sử dụng người dùng đã tồn tại, thì có thể bỏ qua bước này.

Tương tự đối với không gian lưu trữ.

---

### Các thao tác trên NAS
---

#### NAS: Tạo người dùng

Các bước:

1. **Control Panel > User**
2. **Create > Create User**

Tại đây thực hiện các lựa chọn về tên, nhóm, phân quyền của người dùng để hoàn thành.

Tạo mới người dùng để đồng bộ lên từ Elastix. Người dùng này bắt buộc phải đáp ứng:

- Có quyền ghi vào thư mục chỉ định.
- Có quyền sử dụng SSH (\nameref{sec:nas-ssh}).

Thực tế, có thể sử dụng tài khoản admin hiện có của NAS.

**Lưu ý:** Người dùng thuộc nhóm Administrator có quyền SSH.

---

#### NAS: Tạo thư mục

Các bước:

1. **Control Panel > Shared Folder**
2. **Create** hoặc **Edit**

Các tuỳ chọn nên thực hiện:

- Ẩn thư mục trên My Network Places.
- Ẩn thư mục con và tập tin khỏi những người dùng không có quyền.

Lưu ý cấp quyền Đọc+Ghi cho người dùng vừa tạo nhằm vào mục đích chạy sao lưu.

---


#### Terminal & SNMP
\label{sec:nas-ssh}

**Control Panel** > **Terminal & SNMP**:

- **SSH Enabled**.
- Specify port or leave it default (22).

---


### Các thao tác trên Elastix

---


#### Login vào server với tài khoản root

```text
$ ssh -l root elastixht
root@elastixht's password:
```

---

#### Tạo người dùng mới

Bước này tạo mới một người dùng, đồng thời gán mật khẩu, tạo thư mục nhà (`home`) và gán `shell` mặc định.

- Tên (tham số cuối của lệnh dưới): tốt nhất nên trùng với người dùng vừa tạo trên NAS.
- Mật khẩu không bắt buộc tương tự, nhưng có thể dùng chung mật khẩu, dễ nhớ.

```bash
[root@elastixht ~]# useradd -c elastix -m -p daylamatkhau samdx
[root@elastixht ~]# su samdx
[samdx@elastixht ~]$ pwd
/home/samdx
```

---


#### Thử đăng nhập vào NAS

Thử đăng nhập vào NAS với nguời dùng vừa tạo, sử dụng SSH. Mật khẩu là mật khẩu đã tạo trên NAS.

```text
[samdx@centos-01 ~]$ ssh backupht
samdx@backupht's password: 
Last login: Sun Mar 12 17:26:04 2017 from 10.0.0.82


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> 

```

---


## Cấu hình SSH

**Mục đích:** Cài đặt SSH để người dùng có thể đăng nhập sử dụng mã khoá công khai thay vì sử dụng mật khẩu (rightarrow) tiện lợi và an toàn hơn, sẽ không cần parse mật khẩu vào cho script `rsync` dùng để đồng bộ.


**TODO:**

- Tạo mới keygen
- Cho *public key* vừa tạo vào file `~/.ssh/authorized_keys` trên NAS tương ứng người dùng.

---

### Tạo SSH key cho người dùng

Sử dụng `ssh-keygen` như ví dụ dưới: tạo mới một mã khoá RSA.

Mục đích sử dụng *publick key* để đăng nhập vào NAS mà không cần mật khẩu, nên tại dòng *Enter passphrase* sẽ bỏ trống, đơn giản là bấm Enter để tiếp tục.

```bash
[admin@elastixht ~]$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/admin/.ssh/id_rsa): 
Created directory '/home/admin/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/admin/.ssh/id_rsa.
Your public key has been saved in /home/admin/.ssh/id_rsa.pub.
The key fingerprint is:
58:82:5c:ff:65:20:3d:af:1f:87:aa:10:5c:e7:e2:d4 admin@elastixht.htbranch
```
---


### Copy public key vào NAS

#### ssh-copy-id

Sử dụng `ssh-copy-id` theo cú pháp như dưới, mặc định `ssh-copy-id` sẽ sử dụng file `~/.ssh/id_rsa.pub`. Trường hợp muốn chỉ định public key khác, sử dụng tuỳ chọn `-i /path/to/pub_file` khi gọi lệnh.

```text
[samdx@centos-01 ~]$ ssh-copy-id backupht
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s),
to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed --
if you are prompted now it is to install the new keys
samdx@backupht's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'backupht'"
and check to make sure that only the key(s) you wanted were added.

[samdx@centos-01 ~]$
```

---

#### Các cách khác

Ngoài ra, có thể sử dụng những cách khác để thực hiện việc trên. Mục đích là để copy nội dung của public key vào file `~/.ssh/authorized_key` của người dùng tương ứng trên SSH server, ở đây là NAS.

- Từ WEBUI, dùng Text Editor tạo mới và thêm nội dung cho file `authorized_key`.
- SSH vào NAS với người dùng tương ứng và thêm nội dung cho file.
- SSH vào NAS với một tài khoản khác, chuyển đổi người dùng (`su`)và thực hiện việc thêm nội dung cho file.

Hãy lưu ý về phân quyền của thư mục `~/.ssh` và `authorized_key`, các lỗi đăng nhập có thể phát sinh từ đây. Phân quyền 700 cho `~/.ssh` và 600 cho `authorized_key`.

---


### Đăng nhập bằng public key
\label{sub:public_key_signin}

Thử login:

```bash
[samdx@centos-01 ~]$ ssh backupht
Last login: Sun Mar 12 17:42:36 2017 from 10.0.0.82


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> 

```

Như vậy là đã có thể đăng nhập bằng public key, mà không sử dụng trực tiếp mật khẩu của người dùng. Điều này tạo thuận tiện cho việc gọi `rsync` về sau.

---

### A bit tweak for SSHD

Chúng ta sẽ làm một vài thay đổi cho SSHD trên Server, áp dụng cho `sshd` cả trên NAS hay trên Elastix.

- Đổi cổng mặc định.
- Tắt đăng nhập bằng người dùng root.
- Giới hạn người dùng có thể đăng nhập bằng SSH.
- Tắt xác thực bằng mật khẩu, bắt buộc dùng Public key.

Sử dụng `vim` hoặc bất kỳ trình soạn thảo nào bạn thuận tay, và sửa file như dưới:

---

```text
[root@elastixht ~]# vim /etc/sshd/sshd_config
```

```apacheconf
# Use 8822 instead
Port 8822

# Disable root login
RootPermitLogin no

# Allowed users
AllowUsers samdx

PubkeyAuthentication yes

AuthorizedKeysFile .ssh/authorized_keys

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no

# Change to no to disable s/key passwords
ChallengeResponseAuthentication no
```

---

## Elastix Auto-backup

---


### Các công cụ cần thiết

Elastix cung cấp công cụ phục vụ cho việc sao lưu dữ liệu, bao gồm từ cơ sở dữ liệu tới các file ghi âm.

Công cụ này gọi là `backupengine`, nằm tại:

```text
/usr/share/elastix/privileged/backupengine
```

---

Công cụ này định nghĩa các thành phần (*components*) của hệ thống Elastix và thực hiện sao lưu theo chỉ định, các thành phần hoặc mặc định là tất cả.

Đồng thời, Elastix cũng cung cấp công cụ mục đích thực hiện tự động sao lưu, được lưu trữ tại:

```
/var/www/backup/automatic_backup.php
```

Đây là thư mục mặc định chứa các tập tin sao lưu khi thực hiện sao lưu từ WEBUI.

---

### Tuỳ chỉnh công cụ

Chúng ta phải tuỳ chỉnh một chút các công cụ này cho mục đích sao lưu theo ý.

---

#### Tạo thư mục lưu trữ
\label{sub:folder_backup}
Đầu tiên, tạo một thư mục để làm việc. Mục đích lưu trữ các công cụ và các tập tin chứa dữ liệu sao lưu.

Thư mục lưu trữ có thể đặt bất kỳ đâu, tuỳ ý (trừ `/{b,r}oot`). Giả sử đặt trong `/var`.

Do người dùng mới tạo ra không có quyền ghi (`w`) trong thư mục `/var`, nên phải sử dụng quyền `root` của hệ thống để tạo thư mục, sau đó chuyển quyền qua cho người dùng này.

Có thể trực tiếp sử dụng tài khoản `root` hoặc `sudo` (Trường hợp `sudo` phải thêm người dùng vào file `sudoers`, sửa trực tiếp hoặc sử dụng `visudo`).

---

Dưới đây là ví dụ:

```text
[root@elastixht bak]# sudo mkdir /var/backup
[root@elastixht bak]# sudo chown -R samdx: /var/backup/
[root@elastixht bak]# ls -l !$
drwxr-xr-x 2 samdx samdx 4096 Mar 12 18:34 /var/backup/
[root@elastixht bak]#  

```

---

#### Tạo bản sao các công cụ

Sao chép các công cụ ở mục \ref{sec:tools} (\nameref{sec:tools}).

```bash
[samdx@elastixht /var/backup]$ cp /usr/share/elastix/privileged/backupengine .
[samdx@elastixht /var/backup]$ cp /var/www/backup/automatic_backup.php .
```

---

#### Cấp quyền đọc cho tập tin elastix.conf

Tập tin `automatic_backup.php` gọi tập tin `backupengine` để thực hiện sao lưu.

Quá trình sao lưu yêu cầu phải có mật khẩu `root` của MySQL, vốn được lưu trong file `/etc/elastix.conf`.

```php
// backupengine

define('PASSWD_PATH', '/etc/elastix.conf');
```

Tập tin được thiết lập chỉ đọc/ghi đối với người dùng `asterisk` vốn không nằm trong `/etc/group`, nên không thể cấp quyền sử dụng `chmod`.

---

- **Trước:**

```bash
ls -l /etc/elastix.conf 
-rw------- 1 asterisk asterisk 71 Aug 14  2016 /etc/elastix.conf
```

Vì vậy, cần cấp quyền đọc cho người dùng trên file này sử dụng công cụ khác (ACL control):


```bash
$ sudo setfacl -m u:samdx:r /etc/elastix.conf
```

- **Sau:**

```bash
ls -l /etc/elastix.conf
-rw-r-----+ 1 asterisk asterisk 71 Aug 14  2016 /etc/elastix.conf
```

---

### Chỉnh sửa backupengine

Do `backupengine` được giả định sẽ thực thi sử dụng quyền `root`, nên sau khi thực hiện sao lưu, script cần phân quyền lại thư mục/tập tin sao lưu vừa được tạo bởi `root` cho người dùng `asterisk`, tại dòng 716-717 của script, như dưới:

```php
// Switch tarball file permission to asterisk:asterisk
chown($sBackupPath, 'asterisk');
chgrp($sBackupPath, 'asterisk');

```

---

Để loại bỏ hành vi này, vì chúng ta sẽ thực thi script này bằng người dùng bình thường (không `sudo`), đơn giản là ta xoá/comment 2 dòng này, thành:

```php
// Switch tarball file permission to asterisk:asterisk
//chown($sBackupPath, 'asterisk');
//chgrp($sBackupPath, 'asterisk');

```

```bash
$ sed -i '716s+chown+//chown+' backupengine
$ sed -i '717s+chgrp+//chgrp+' backupengine

```

--

### Chỉnh sửa automatic_backup.php

Mặc định, script này sẽ gọi file `backupengine` trên từ trong `/usr/share`, và thư mục sao lưu mặc định là `/var/www/backup`.

```php
$sBackupDir = '/var/www/backup';

system('/usr/share/elastix/privileged/backupengine --backup --backupfile '. );
```

---

Một vài thay đổi:

- Thư mục sao lưu mới là `/var/backup/elastix_backup`
- `backupengine` đã sửa nằm trong `PATH` khi đưa vào `crond` (mục \ref{sec:crontab} trang \pageref{sec:crontab}) sau này, nên dùng đường dẫn tuyệt đối hoặc không là tuỳ chọn.
- Chỉ định các thành phần (`components`) cần sao lưu, thay vì sao lưu tất cả như trên.

---

Vì vậy, `automatic_backup.php` sau khi đã sửa sẽ như dưới. Các thành phần được định nghĩa trong `backupengine`. Có thể lựa chọn tất cả ngoại từ `as_monitor` và `as_voicemail` vì mục sau (\nameref{sec:rsync} trang \pageref{sec:rsync}) sẽ thực hiện sao lưu các dữ liệu nặng này.

```php
// automatic_backup.php

$sBackupDir = '/var/backup/elastix_backup';

system('backupengine --backup --components as_db,as_config_files --backupfile '. );

```

---

## Đồng bộ với RSync

Mục này sẽ thực hiện đồng bộ tất cả những dữ liệu cần sao lưu lên server khác, trường hợp này là NAS.

Ở mục chúng ta đã tạo thư mục dành cho công việc sao lưu chúng ta đã chuẩn bị các công cụ cho việc sao lưu.

Ta chuyển thư mục làm việc đến `/var/backup/` cùng các công cụ ở mục trước.

Các việc tiến hành:

- Thử `rsync` với public key đã có.
- Lên danh sách các thư mục/item cần sao lưu.
- Tạo script cho việc sao lưu.

---

### Thử rsync

Sau khi có thể `ssh` sử dụng public key (\nameref{sub:public_key_signin} trang \pageref{sub:public_key_signin}), thử sử dụng `rsync` để đảm bảo rằng không lỗi nào phát sinh liên quan đến việc xác thực khi đăng nhập.

---

```text
[samdx@elastixht /var/backup]$ echo -e "## WELCOME\n" > README.md
[samdx@elastixht /var/backup]$ rsync -av README.md backupht:/volume1/elastix-bak/
sending incremental file list
README.md

sent 104 bytes  received 31 bytes  90.00 bytes/sec
total size is 12  speedup is 0.09
[samdx@elastixht /var/backup]$ ssh backupht
Last login: Sun Mar 12 18:23:42 2017 from 10.0.0.12


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> ls /volume1/elastix-bak/
README.md
BackupHT> ls -l /volume1/elastix-bak/
total 8
-rw-rw-r-- 1 samdx users   12 2017-03-12 18:28 README.md

```

---

### Các thư mục cần sao lưu

Danh sách các thư mục cần sao lưu, sẽ được cho vào một tập tin, tạm gọi là `included-items.txt`.

Mục đích sao lưu những thành phần của Elastix, nội dung tập tin sẽ tương tự:

```
/var/spool/asterisk/monitor
/var/www/backup
/var/log/asterisk/cdr-csv
/var/backup

```

---

Trong đó:

- `/var/spool/asterisk/monitor`: Thư mục chứa các bản ghi âm, thường rất nặng.
- `/var/www/backup`: Những tập tin sao lưu được lệnh từ WEBGUI sẽ lưu tại đây.
- `/var/log/asterisk/cdr-csv`: Bản ghi danh sách cuộc gọi, từ bảng `cdr` của `asteriskcdrdb`.
- `/var/backup`: Thư mục hiện hành, bao gồm các công cụ sao lưu và các bản sao lưu được tạo bởi `automatic_backup.php` đã nói ở trên.

Có thể thêm các thành phần nằm trong thư mục `/var/spool/asterisk`, ví dụ `voicemaill` nếu cần.

---

### Kịch bản sao lưu

Mục này chúng ta tạo thư mục lưu các kịch bản và các file dành cho việc sao lưu.

- Tạo thư mục `bin` trong `/var/backup`.
- Tạo mới file `automatic_sync.sh` trong thư mục trên.
- Di chuyển các tập tin bao gồm `automatic_backup.php`, `backupengine`, và `included-items.txt`.

```
[samdx@elastixht /var/backup]$ mkdỉr bin
[samdx@elastixht /var/backup]$ mv *.{php,txt} bin/
[samdx@elastixht /var/backup]$ vim bin/automatic_sync.sh
```

---


#### automatic_sync.sh

Tập tin kịch bản này thực hiện việc đồng bộ các thư mục chỉ định đến đích là một thư mục trên NAS đã được định trước, đủ quyền đọc/ghi.

```bash
#!/bin/env bash

# THIS VERY SIMPLE AND UGLY SCRIPT

# Chỉ định tên người dùng và hostname của NAS
SSH_USER=samdx
SSH_SERVER=backupht
```

---

```bash
# Các tuỳ chọn cho rsync:
# Thư mục lưu trên NAS
# Giới hạn băng thông nếu cần (Hà Tĩnh 100MB based)
# Các tuỳ chọn khác nếu có
# The destination is on the SSH_SERVER not local
RSYNC_DEST_DIR="volume1/elastix-bak"
RSYNC_BWLIMIT="5120" # 5MB, unit in KBPS
RSYNC_OPT=""

if [ -n $RSYNC_BWLIMIT ]
then
    RSYNC_OPT="${RSYNC_OPT} --bwlimit=$RSYNC_BWLIMIT"
fi
```

---

```bash
# Get current datetime
CURRENT_TIME=$(date +%Y-%m-%d-%H-%M-%Z)

# The path where this script and/or some other file lives on
BACK_DIR="/var/backup"
# The folder that holds rsync log files
SYNC_LOG_DIR="synclog"

# Specify the file that contains all the location need to be synced
INCLUDE_FILE_NAME="included-items.txt"
INCLUDE_FILE=$BACK_DIR/bin/$INCLUDE_FILE_NAME

# The sync log file name and path
SYNC_LOG_FILE="$BACK_DIR/$SYNC_LOG_DIR/$CURRENT_TIME.log"

# Then sync
rsync -ar $RSYNC_OPT --files-from=$INCLUDE_FILE \
/ $SSH_USER@$SSH_SERVER:/$RSYNC_DEST_DIR \
> $SYNC_LOG_FILE 2>&1

```

---

## Crontab

Sau khi các thành phần khác đã sẵn sàng, ta sử dụng `crontab` để lên lịch đồng bộ tự động.

Xác định thời điểm cần thực hiện sao lưu và đồng bộ, sau đó chỉnh sửa với `crontab -e`. `crontab` gọi `vi` để sửa tập tin, `:wq` để lưu và thoát.

```
PATH=/var/backup/bin:/bin:/usr/bin:/usr/local/bin
SHELL=/bin/bash
MAILTO=""
0 19 * * * automatic_sync
0 12 * * * automatic_backup

```

---

Trong đó:

- `PATH`: chỉ định các đường dẫn chứa các tập tin thực thi mà đã được chuẩn bị ở trên, bao gồm `/var/backup/bin`.
- SHELL: chỉ định `shell` sẽ được sử dụng, mặc định `crontab` sử dụng `/bin/sh`.
- Bên dưới là lịch và lệnh gọi các công cụ tương ứng. Đồng bộ vào lúc 19h mỗi ngày, sao lưu vào lúc 12h mỗi ngày.

Các tập tin kịch bản đã được đổi tên và loại bỏ phần mở rộng. Chúng ta không cần lo lắng, *Linux* sẽ nhận dạng dựa trên kiểu `MIME` và `header` của tập tin.

---

### Các ví dụ về lập lịch

```
@daily
```

- Mỗi ngày, tính từ lúc thêm vào lịch.


```
0 */2 * * *
```

- Mỗi 2 tiếng một, tất cả các ngày.

```
0 0 * * 6
```

- Vào lúc 00:00 mỗi thứ 7.


```
0 12 * */3 SAT
```

- Vào lúc 12:00 thứ 7 mỗi 3 tháng một (theo quý). Đặt \today{} đến 01/04 là thứ 7 kế tiếp thực thi.

---

## TODO

Các nội dung khác cần hoàn thiện và bổ sung, như:

- Email thông báo mỗi khi sao lưu/đồng bộ xong. Tuỳ loại sao lưu, theo ngày, tuần.
- Tìm kiếm, loại bỏ hoặc phân tách các gói sao lưu quá lâu một khoảng thời gian nào đó (60 ngày chẳng hạn), xoá hoặc thông báo cho Admin.
- Tương tự đối với các tập tin log của kịch bản đồng bộ.

---

## THE END

[`@samdx`](mailto:samdx@hongha.asia)
