## TODO

Các nội dung khác cần hoàn thiện và bổ sung, như:

- Email thông báo mỗi khi sao lưu/đồng bộ xong. Tuỳ loại sao lưu, theo ngày, tuần.
- Tìm kiếm, loại bỏ hoặc phân tách các gói sao lưu quá lâu một khoảng thời gian nào đó (60 ngày chẳng hạn), xoá hoặc thông báo cho Admin.
- Tương tự đối với các tập tin log của kịch bản đồng bộ.
