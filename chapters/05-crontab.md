## Crontab
\label{sec:crontab}

Sau khi các thành phần khác đã sẵn sàng, ta sử dụng `crontab` để lên lịch đồng bộ tự động.

Xác định thời điểm cần thực hiện sao lưu và đồng bộ, sau đó chỉnh sửa với `crontab -e`. `crontab` gọi `vi` để sửa tập tin, `:wq` để lưu và thoát.

```
PATH=/var/backup/bin:/bin:/usr/bin:/usr/local/bin
SHELL=/bin/bash
MAILTO=""
0 19 * * * automatic_sync
0 12 * * * automatic_backup

```

Trong đó:

- `PATH`: chỉ định các đường dẫn chứa các tập tin thực thi mà đã được chuẩn bị ở trên, bao gồm `/var/backup/bin`.
- SHELL: chỉ định `shell` sẽ được sử dụng, mặc định `crontab` sử dụng `/bin/sh`.
- Bên dưới là lịch và lệnh gọi các công cụ tương ứng. Đồng bộ vào lúc 19h mỗi ngày, sao lưu vào lúc 12h mỗi ngày.

Các tập tin kịch bản đã được đổi tên và loại bỏ phần mở rộng. Chúng ta không cần lo lắng, *Linux* sẽ nhận dạng dựa trên kiểu `MIME` và `header` của tập tin.

### Các ví dụ về lập lịch

```
@daily
```

- Mỗi ngày, tính từ lúc thêm vào lịch.


```
0 */2 * * *
```

- Mỗi 2 tiếng một, tất cả các ngày.

```
0 0 * * 6
```

- Vào lúc 00:00 mỗi thứ 7.

```
0 12 * */3 SAT
```

- Vào lúc 12:00 thứ 7 mỗi 3 tháng một (theo quý). Đặt \today{} đến 01/04 là thứ 7 kế tiếp thực thi.

