## Chuẩn bị người dùng và thư mục lưu trữ

Tại mục này chúng ta sẽ hoàn thành việc khởi tạo người dùng mới cho mục đích chạy tác vụ sao lưu. Thực tế, có thể sử dụng ngay tài khoản root trên hệ thống, nhưng điều này có thể không tiện. Tất nhiên cũng có thể sử dụng người dùng đã tồn tại, thì có thể bỏ qua bước này.

Tương tự đối với không gian lưu trữ.

### Các thao tác trên NAS

#### NAS: Tạo người dùng

Các bước:

1. **Control Panel > User**
2. **Create > Create User**

Tại đây thực hiện các lựa chọn về tên, nhóm, phân quyền của người dùng để hoàn thành.

Tạo mới người dùng để đồng bộ lên từ Elastix. Người dùng này bắt buộc phải đáp ứng:

- Có quyền ghi vào thư mục chỉ định.
- Có quyền sử dụng SSH (\nameref{sec:nas-ssh}).

Thực tế, có thể sử dụng tài khoản admin hiện có của NAS.

**Lưu ý:** Người dùng thuộc nhóm Administrator có quyền SSH.

#### NAS: Tạo thư mục

Các bước:

1. **Control Panel > Shared Folder**
2. **Create** hoặc **Edit**

Các tuỳ chọn nên thực hiện:

- Ẩn thư mục trên My Network Places.
- Ẩn thư mục con và tập tin khỏi những người dùng không có quyền.

Lưu ý cấp quyền Đọc+Ghi cho người dùng vừa tạo nhằm vào mục đích chạy sao lưu.

#### Terminal & SNMP
\label{sec:nas-ssh}

**Control Panel** > **Terminal & SNMP**:

- **SSH Enabled**.
- Specify port or leave it default (22).

### Các thao tác trên Elastix

#### Login vào server với tài khoản root

```text
$ ssh -l root elastixht
root@elastixht's password:
```

#### Tạo người dùng mới

Bước này tạo mới một người dùng, đồng thời gán mật khẩu, tạo thư mục nhà (`home`) và gán `shell` mặc định.

- Tên (tham số cuối của lệnh dưới): tốt nhất nên trùng với người dùng vừa tạo trên NAS.
- Mật khẩu không bắt buộc tương tự, nhưng có thể dùng chung mật khẩu, dễ nhớ.

```text
[root@elastixht ~]# useradd -c elastix -m -p daylamatkhau samdx
[root@elastixht ~]# su samdx
[samdx@elastixht ~]$ pwd
/home/samdx
```

#### Thử đăng nhập vào NAS

Thử đăng nhập vào NAS với nguời dùng vừa tạo, sử dụng SSH. Mật khẩu là mật khẩu đã tạo trên NAS.

```text
[samdx@centos-01 ~]$ ssh backupht
samdx@backupht's password: 
Last login: Sun Mar 12 17:26:04 2017 from 10.0.0.82


BusyBox v1.16.1 (2015-05-12 15:50:32 CST) built-in shell (ash)
Enter 'help' for a list of built-in commands.

BackupHT> 

```
