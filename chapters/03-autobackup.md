## Elastix Auto-backup

### Các công cụ cần thiết
\label{sec:tools}

Elastix cung cấp công cụ phục vụ cho việc sao lưu dữ liệu, bao gồm từ cơ sở dữ liệu tới các file ghi âm.

Công cụ này gọi là `backupengine`, nằm tại:

```text
/usr/share/elastix/privileged/backupengine
```

Công cụ này định nghĩa các thành phần (*components*) của hệ thống Elastix và thực hiện sao lưu theo chỉ định, các thành phần hoặc mặc định là tất cả.

Đồng thời, Elastix cũng cung cấp công cụ mục đích thực hiện tự động sao lưu, được lưu trữ tại:

```
/var/www/backup/automatic_backup.php
```

Đây là thư mục mặc định chứa các tập tin sao lưu khi thực hiện sao lưu từ WEBUI.

### Tuỳ chỉnh công cụ

Chúng ta phải tuỳ chỉnh một chút các công cụ này cho mục đích sao lưu theo ý.

#### Tạo thư mục lưu trữ
\label{sub:folder_backup}
Đầu tiên, tạo một thư mục để làm việc. Mục đích lưu trữ các công cụ và các tập tin chứa dữ liệu sao lưu.

Thư mục lưu trữ có thể đặt bất kỳ đâu, tuỳ ý (trừ `/{b,r}oot`). Giả sử đặt trong `/var`.

Do người dùng mới tạo ra không có quyền ghi (`w`) trong thư mục `/var`, nên phải sử dụng quyền `root` của hệ thống để tạo thư mục, sau đó chuyển quyền qua cho người dùng này.

Có thể trực tiếp sử dụng tài khoản `root` hoặc `sudo` (Trường hợp `sudo` phải thêm người dùng vào file `sudoers`, sửa trực tiếp hoặc sử dụng `visudo`).

Dưới đây là ví dụ:

```text
[root@elastixht bak]# sudo mkdir /var/backup
[root@elastixht bak]# sudo chown -R samdx: /var/backup/
[root@elastixht bak]# ls -l !$
drwxr-xr-x 2 samdx samdx 4096 Mar 12 18:34 /var/backup/
[root@elastixht bak]#  

```

#### Tạo bản sao các công cụ

Sao chép các công cụ ở mục \ref{sec:tools} (\nameref{sec:tools}).

```bash
[samdx@elastixht /var/backup]$ cp /usr/share/elastix/privileged/backupengine .
[samdx@elastixht /var/backup]$ cp /var/www/backup/automatic_backup.php .
```

#### Cấp quyền đọc cho tập tin elastix.conf

Tập tin `automatic_backup.php` gọi tập tin `backupengine` để thực hiện sao lưu.

Quá trình sao lưu yêu cầu phải có mật khẩu `root` của MySQL, vốn được lưu trong file `/etc/elastix.conf`.

```php
// backupengine

define('PASSWD_PATH', '/etc/elastix.conf');
```

Tập tin được thiết lập chỉ đọc/ghi đối với người dùng `asterisk` vốn không nằm trong `/etc/group`, nên không thể cấp quyền sử dụng `chmod`.

- **Trước:**

```bash
ls -l /etc/elastix.conf 
-rw------- 1 asterisk asterisk 71 Aug 14  2016 /etc/elastix.conf
```

Vì vậy, cần cấp quyền đọc cho người dùng trên file này sử dụng công cụ khác (ACL control):


```bash
$ sudo setfacl -m u:samdx:r /etc/elastix.conf
```

- **Sau:**

```bash
ls -l /etc/elastix.conf
-rw-r-----+ 1 asterisk asterisk 71 Aug 14  2016 /etc/elastix.conf
```

### Chỉnh sửa backupengine

Do `backupengine` được giả định sẽ thực thi sử dụng quyền `root`, nên sau khi thực hiện sao lưu, script cần phân quyền lại thư mục/tập tin sao lưu vừa được tạo bởi `root` cho người dùng `asterisk`, tại dòng 716--717 của script, như dưới:

```php
// Switch tarball file permission to asterisk:asterisk
chown($sBackupPath, 'asterisk');
chgrp($sBackupPath, 'asterisk');

```

Để loại bỏ hành vi này, vì chúng ta sẽ thực thi script này bằng người dùng bình thường (không `sudo`), đơn giản là ta xoá/comment 2 dòng này, thành:

```php
// Switch tarball file permission to asterisk:asterisk
//chown($sBackupPath, 'asterisk');
//chgrp($sBackupPath, 'asterisk');

```

```bash
$ sed -i '716s+chown+//chown+' backupengine
$ sed -i '717s+chgrp+//chgrp+' backupengine

```

### Chỉnh sửa automatic_backup.php

Mặc định, script này sẽ gọi file `backupengine` trên từ trong `/usr/share`, và thư mục sao lưu mặc định là `/var/www/backup`.

```php
$sBackupDir = '/var/www/backup';

system('/usr/share/elastix/privileged/backupengine --backup --backupfile '. );
```

Một vài thay đổi:

- Thư mục sao lưu mới là `/var/backup/elastix_backup`
- `backupengine` đã sửa nằm trong `PATH` khi đưa vào `crond` (mục \ref{sec:crontab} trang \pageref{sec:crontab}) sau này, nên dùng đường dẫn tuyệt đối hoặc không là tuỳ chọn.
- Chỉ định các thành phần (`components`) cần sao lưu, thay vì sao lưu tất cả như trên.

Vì vậy, `automatic_backup.php` sau khi đã sửa sẽ như dưới. Các thành phần được định nghĩa trong `backupengine`. Có thể lựa chọn tất cả ngoại từ `as_monitor` và `as_voicemail` vì mục sau (\nameref{sec:rsync} trang \pageref{sec:rsync}) sẽ thực hiện sao lưu các dữ liệu nặng này.

```php
// automatic_backup.php

$sBackupDir = '/var/backup/elastix_backup';

system('backupengine --backup --components as_db,as_config_files --backupfile '. );

```
